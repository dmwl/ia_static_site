import React from 'react'
import { Container } from 'react-responsive-grid'
import { Link } from 'react-router'
import { prefixLink } from 'gatsby-helpers'
import Headroom from 'react-headroom'
import '../css/markdown-styles'
import '../css/menu.scss'
import '../css/misc.scss'

import { rhythm } from '../utils/typography'

module.exports = React.createClass({
  propTypes () {
    return {
      children: React.PropTypes.any,
    }
  },
  render () {
    return (
      <div>
        <Headroom
          className='headroom'
        >
          <Container
            style={{
              maxWidth: 960,
              paddingTop: 0,
              padding: `${rhythm(1/2)} ${rhythm(3/4)}`,
            }}
          >
            <Link
              to={prefixLink('/')}
              style={{
                color: 'black',
                textDecoration: 'none',
                float: 'left',
              }}
            >
              Islander Access
              <div
                style={{
                  marginTop: -10,
                }}
              >
                <small>
                  Experience Trinidad and Tobago Carnival
                </small>
              </div>
            </Link>

            <div className="nav">
              <ul>				
				<li>
                  <Link
                    to={prefixLink('/faq/')}
                  >
                    FAQ
                  </Link>
                </li>
                <li>
                  <Link
                    to={prefixLink('/about-us/')}
                  >
                    About Us
                  </Link>
                </li>
                <li>
                  <Link
                    to={prefixLink('/sell-your-tickets/')}
                  >
                    Sell Your Tickets
                  </Link>
                </li>
              </ul>
            </div>
          </Container>
        </Headroom>
        <Container
          style={{
            maxWidth: 960,
            padding: `${rhythm(1)} ${rhythm(3/4)}`,
            paddingTop: 0,
          }}
        >
          {this.props.children}
        </Container>
        <footer>
          <Container
            style={{
              maxWidth: 960,
              padding: `${rhythm(1/2)} ${rhythm(3/4)}`,
              paddingTop: `${rhythm(1/2)}`,
              paddingBottom: 0,
            }}
          >
            <ul>
              <li>
                <Link
                  to={prefixLink('/privacy-policy/')}
                >
                  Privacy Policy
                </Link>
              </li>
              <li>
                <Link
                  to={prefixLink('/terms-and-conditions/')}
                >
                  Terms and Conditions
                </Link>
              </li>
              <li>
                <Link
                  to={prefixLink('/contact-us/')}
                >
                  Contact Us
                </Link>
              </li>
              <li>
                <Link
                  to={prefixLink('/carnival-information/')}
                >
                  Carnival Information
                </Link>
              </li>
            </ul>
          </Container>
        </footer>
      </div>
    )
  },
})
