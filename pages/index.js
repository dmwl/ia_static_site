import React from 'react'
import { Link } from 'react-router'
import { prefixLink } from 'gatsby-helpers'
import Helmet from "react-helmet"
import { config } from 'config'
import './home'

export default class Index extends React.Component {
  render () {
    return (
      <div>
        <Helmet
          title={config.siteTitle}
          meta={[
            {"name": "description", "content": "Islander Access"},
            {"name": "keywords", "content": "events Trinidad Tobago"},
          ]}
        />
        <h1>Upcoming Events</h1>
        <a href="https://tickets.islanderaccess.com/event/fatimafete" >
          <img src="fatima-fete.jpg" alt="Exhale - Fatima Old Boys Association" height="342"/>
        </a>
        <div
          className="nav"
          style={{
            float: "none",
            marginTop: -20,
          }}
        >
          <ul>
            <li>
              <Link
                to={prefixLink('/fatima-fete/')}
              >
                more info
              </Link>
            </li>
            <li>
              <a href="https://tickets.islanderaccess.com/event/fatimafete" >
                buy now
              </a>
            </li>
          </ul>
        </div>
      </div>
    )
  }
}
